import java.util.*

class Demo(
    private val simulationService: SimulationService = CrossroadSimulationServiceImpl()
) {
    fun run() {
        val simulation = simulationService.loadSimulation(emptyMap())
        simulationService.executeSimulation(simulation)
    }
}

class SimulationSchedulerImpl : SimulationScheduler {
    override fun scheduleEvent(event: Event<SimulationContext>, simulation: Simulation): Boolean {
        TODO("Not yet implemented")
    }

    override fun getNextEvent(simulation: PriorityQueue<Event<SimulationContext>>): Event<SimulationContext> {
        TODO("Not yet implemented")
    }

}

class CrossroadSimulationServiceImpl(
    private val scheduler: SimulationScheduler = SimulationSchedulerImpl()
) : SimulationService {
    override fun loadSimulation(configuration: Map<String, String>): Simulation {
        TODO("Not yet implemented")
    }

    override fun executeSimulation(simulation: Simulation) {
        while (simulation.events.size > 0) {
            scheduler
                .getNextEvent(simulation.events)
                .execute(simulation.simulationContext)
        }
        printSimulationStats()
    }

    /**
     * Prints simulation context
     */
    private fun printSimulationStats() {
        TODO("Not yet implemented")
    }
}
