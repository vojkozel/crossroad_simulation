import java.util.*

/**
 * Represents Event entity.
 * Event happens at specific time in simulation discrete timeline.
 * Executing event should change SimulationContext or plan another Events in future based on its behaviour.
 */
interface Event<T : SimulationContext> {
    /**
     * Each event has to have specific execution time
     */
    val executionTime: Int

    /**
     * Priority of event. It can be used if we want to order events that happen at same sime
     * (ie. Semaphore traffic light switch has higher priority than car leave event).
     */
    val priority: Int

    /**
     * Implementation should have behaviour that is specific to [Event] implementation.
     * It should change [SimulationContext].
     * (ie. CarArriveEvent should add new [Car] instance to [Semaphore] car queue and plan next CarArriveEvent)
     *
     * @param context simulation state
     */
    fun execute(context: T): List<Event<T>>

    /**
     * Provides possibility to order Events by time and priority.
     */
    companion object : Comparator<Event<SimulationContext>> {
        override fun compare(a: Event<SimulationContext>, b: Event<SimulationContext>): Int {
            return if (a.executionTime != b.executionTime) {
                a.executionTime - b.executionTime
            } else {
                a.priority - b.priority
            }
        }
    }
}

interface SimulationScheduler {
    /**
     * It schedules event to [simulation] entity.
     * It is also responsible for basic validation (can't schedule to past time or after end of simulation window
     * specified in [Simulation] object)
     */
    fun scheduleEvent(event: Event<SimulationContext>, simulation: Simulation): Boolean

    /**
     * Gets next event from [Simulation] event queue.
     * It is responsible for providing event in correct order (by execution time and events priorities)
     */
    fun getNextEvent(simulation: PriorityQueue<Event<SimulationContext>>): Event<SimulationContext>
}


/**
 * Represents SimulationContext.
 * Implementation should be whatever you want (ie. Crossroad, Shop, CarFactory...)
 * [Event] entity is responsible for reading specific implementations
 *
 * @see Event
 */
interface SimulationContext {}

/**
 * Service responsible for orchestrating simulation process.
 */
interface SimulationService {
    /**
     * It parses input [configuration] and constructs [Simulation] entity.
     * It is responsible for setting [Simulation] to initial state (ie. scheduling initial set of [Event])
     */
    fun loadSimulation(configuration: Map<String, String>): Simulation

    /**
     * Executes simulation.
     * It means that it just call [SimulationScheduler] in infinite loop until [Simulation] event queue is empty
     * After queue is empty, it prints information about [Simulation] state (ie. [SimulationContext])
     */
    fun executeSimulation(simulation: Simulation)
}