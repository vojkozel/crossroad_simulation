import java.util.*

/**
 * Wrapper for whole simulation.
 *
 * @param events represent simulation timeline (correct order provided by PriorityQueue and overridden [Event] comparator)
 * @param length simulation length
 * @param currentTime current state of simulation, it can be changed during simulation execution
 * @param simulationContext holds important simulation data (ie. Crossroad state)
 */
data class Simulation(
    var events: PriorityQueue<Event<SimulationContext>>,
    val length: Int,
    var currentTime: Int,
    val simulationContext: SimulationContext
)

/**
 * Crossroad represents [SimulationContext]
 * It is major part of simulation, execution of [Event] behaves according [Semaphore] state
 *
 * @param endpoints are crossroad input's
 */
data class Crossroad(
    val endpoints: Map<Direction, Semaphore>
) : SimulationContext

/**
 * Represents semaphore state.
 * Semaphore is located at every crossroad endpoint.
 *
 * @param cars queue of cars that are waiting for leaving crossroad
 * @param greenLight simplified state of traffic lights (green/red)
 */
data class Semaphore(
    val cars: Queue<Car>,
    var greenLight: Boolean
)

/**
 * Represents unique car instance
 *
 * @param id auto-generated unique identifier of [Car]
 */
data class Car(
    val id: UUID = UUID.randomUUID()
)

/**
 * Direction enum class specifies endpoint of [Crossroad].
 * In case of extending [Crossroad] (i.e. adding 2 endpoints) this class has to be extended
 */
enum class Direction {
    NORTH, SOUTH, EAST, WEST
}