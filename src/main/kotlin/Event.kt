/**
 * Represents car generator. Once Event was created and put into event queue, it will
 * automatically generate car every [spawnDelay] at specific [direction]
 *
 * @param direction specifies endpoint of spawned car
 * @param spawnDelay defines when next spawn car event will happen
 */
class CarSpawnedEvent(
    override val executionTime: Int,
    override val priority: Int,
    private val direction: Direction,
    private val spawnDelay: Int //expected value of A1,A2,A3,A4 constants
) : Event<Crossroad> {
    override fun execute(context: Crossroad): List<Event<Crossroad>> {
        TODO("Not yet implemented")
        // Put new [Car] instance into context[direction].cars queue

        // returned events:
        // 1. CarLeaveEvent (if context[direction].cars.size = 1) where:
        //      - executionTime = this.executionTime
        //      - direction = this.direction
        //      - priority = 1
        // 2. CarSpawnedEvent where:
        //      - executionTime = this.executionTime + this.spawnDelay
        //      - priority = 1
        //      - direction = this.direction
        //      - spawnDelay = this.spawnDelay
    }
}

/**
 * Represent situation that car wants to start crossing crossroad.
 *
 * @param direction specified endpoint of [Crossroad] where [Car] want's to leave
 * @param nextLeaveDelay schedules next [CarLeaveEvent] (simulates crossroad throughput)
 */
class CarLeaveEvent(
    private val direction: Direction,
    private val nextLeaveDelay: Int = 1, //default value represents S1 constant
    override val executionTime: Int,
    override val priority: Int
) : Event<Crossroad> {
    override fun execute(context: Crossroad): List<Event<Crossroad>> {
        TODO("Not yet implemented")
        //  Check if context[direction].greenLight is true:
        //      yes)
        //          - remove Car from context[direction].cars queue
        //          - plan CarLeaveEvent to (executionTime + nextLeaveDelay)
        //      no)
        //          - nothing to do, skipped event, we can't leave

        // returned events:
        // 1. CarLeaveEvent if we have green light where:
        //      - executionTime = this.executionTime + this.nextLeaveDelay
        //      - direction = this.direction
        //      - priority = this.priority
    }
}

/**
 * Represents situation that traffic light switched their state (green->red or red->green)
 *
 * It changes state of all traffic lights, creates CarLeaveEvent
 * for every traffic light that has green color and schedule next [TrafficLightSwitchEvent]
 *
 * @param nextSwitchDelay specifies when next TrafficLightSwitchEvent event will happen
 */
class TrafficLightSwitchEvent(
    override val executionTime: Int,
    override val priority: Int,
    private val nextSwitchDelay: Int //represents X1/X2 constants
): Event<Crossroad> {
    override fun execute(context: Crossroad): List<Event<Crossroad>> {
        TODO("Not yet implemented")
        // Change state of all context.crossroad.endpoints[].greenLight
        // For every traffic light that has green color (new state) create CarLeaveEvent to executionTime

        // returned events:
        // 1. CarLeaveEvent for all endpoints with green light semaphores where:
        //      - executionTime = this.executionTime
        //      - priority = 1
        //      - direction = direction of green light semaphores for which we create event
        // 2. TrafficLightSwitchEvent to (executionTime + nextSwitchDelay) where:
        //      - nextSwitchDelay = if(this.nextSwitchDelay = X1) { X2 } else { X1 }
        //      - executionTime = this.executionTime + nextSwitchDelay
        //      - priority = 1
    }
}